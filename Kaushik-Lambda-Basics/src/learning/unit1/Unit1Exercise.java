package learning.unit1;

import java.util.Arrays;
import java.util.List;

public class Unit1Exercise {
    public static void main(String[] args) {

        List<Person> people = Arrays.asList(
                new Person("charles", "dickens", 60),
                new Person("lewis", "carroll", 42),
                new Person("thomas", "carlyle", 51),
                new Person("charlotte", "bronte", 45),
                new Person("Matthew", "Arnold", 39)
                );

        // Step 1: Sort list by last name

        // Step 2: Create a method that prints all the elements in the list

        // Step 3: Create a method that prints all people that have last name beginning with C
    }
}

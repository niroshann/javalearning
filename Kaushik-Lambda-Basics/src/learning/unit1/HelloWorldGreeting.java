package learning.unit1;

public class HelloWorldGreeting implements Greeting{

    @Override
    public void perform() {
        System.out.println("Printing from Greeting class");
    }
}

package learning.unit2;

public class ThisReferrenceExample {
    public void doProcess(int i, Process p){
        p.process(i);
    }

    public static void main(String[] args) {
        ThisReferrenceExample thisReferrenceExample = new ThisReferrenceExample();

        thisReferrenceExample.doProcess(10, i -> {
            System.out.println("Value of i is " + i);
            //System.out.println(this); // this will not work because
        });
    }
}

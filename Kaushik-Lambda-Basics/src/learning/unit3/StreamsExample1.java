package learning.unit3;

import learning.unit1.Person;

import java.util.Arrays;
import java.util.List;

public class StreamsExample1 {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("charles", "dickens", 60),
                new Person("lewis", "carroll", 42),
                new Person("thomas", "carlyle", 51),
                new Person("charlotte", "bronte", 45),
                new Person("Matthew", "Arnold", 39)
        );

        people.stream()
                .filter(p -> p.getLastName().startsWith("c"))
                .forEach(p -> System.out.println(p.getFirstName()));

        long count = people.parallelStream()
                .filter(p -> p.getLastName().startsWith("c"))
                .count();

        System.out.println(count);
    }
}

package learning.unit3;

import learning.unit1.Person;

import java.util.Arrays;
import java.util.List;

public class CollectionIterationExample {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("charles", "dickens", 60),
                new Person("lewis", "carroll", 42),
                new Person("thomas", "carlyle", 51),
                new Person("charlotte", "bronte", 45),
                new Person("Matthew", "Arnold", 39)
        );


        //Reqular for loo;
        System.out.println("Regular for loop example" + "\n" );
        for (int i = 0; i < people.size(); i++) {
            System.out.println(people.get(i));
        }

        // For in loop
        System.out.println("\n" + "**********************" + "\n" +"For in loop example" + "\n");
        for (Person p:
             people) {
            System.out.println(p);
        }

        System.out.println("\n" + "**********************" + "\n" +"For each lambda loop example" + "\n");
        people.forEach(System.out::println);
    }
}

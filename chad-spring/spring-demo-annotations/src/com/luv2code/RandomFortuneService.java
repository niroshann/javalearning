package com.luv2code;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Random;

@Component
public class RandomFortuneService implements FortuneService {

    private String[] data = {
            "Beware of the wolf in the sheeps clothing",
            "Diligence is the mother of good luck",
            "The journey is the reward"
    };

    private Random myRandom = new Random();


    @Override
    public String getFortune() {

        int index = myRandom.nextInt(data.length);
        String theFortune = data[index];
        return theFortune;
    }

    @PostConstruct
    public void PostConstructTestInRandomFortuneService(){
        System.out.println(">> RandomFortuneService: inside method PostConstructTestInRandomFortuneService()");
    }


    @PreDestroy
    public void PreDestroyTestInRandomFortuneService(){
        System.out.println(">> RandomFortuneService: inside method PreDestroyTestInRandomFortuneService()");
    }
}

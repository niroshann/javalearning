<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
    <title>Student confirmation</title>
</head>
<body>

    <%-- Calls student.getFirstName() student.getLastName --%>
    The student is confirmed: ${student.firstName} ${student.lastName} <br>

    <%--This is jsp expression language--%>
    Country: ${student.country} <br>

    Favourite language: ${student.favouriteLanguage} <br>

    Operating Systems:
    <ul>
        <c:forEach var="temp" items="${student.operatingSystems}">
            <li> ${temp} </li>
        </c:forEach>
    </ul>

</body>
</html>
<%--Below is the referrence for spring tag form library--%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
<head>
    <title>Student registration form</title>
</head>
<body>

    <form:form action="processForm" modelAttribute="student">

        <%--When form is loaded Spring mvc will call getFirstName() and getLastName()--%>
        First name: <form:input path="firstName" /> <br>
        
        Last name: <form:input path="lastName" /> <br>
        
        country:

        <%--During form submission, spring will call the student.setCountry--%>
            <%--This example is for creating options hardcoded--%>

            <%--<form:select path="country">
                <form:option value="Brazil" label="Brazil" />
                <form:option value="France" label="France" />
                <form:option value="Germany" label="Germany" />
                <form:option value="India" label="India" />
            </form:select>--%>

        <%--During form submission, spring will call the student.setCountryOptions--%>
        <%--Here 'items' refer to the collection of data--%>
        <%--spring will call student.getCountryOptions()--%>

            <form:select path="country">
                <form:options items="${student.countryOptions}" />
            </form:select> <br>

        Favourite language:
            <form:radiobuttons path="favouriteLanguage" items="${student.favouriteLanguages}"/>
        <br>

        Favourite Operating systems:
            Linux <form:checkbox path="operatingSystems" value="linux" />
            Mac OS <form:checkbox path="operatingSystems" value="mac os" />
            Windows <form:checkbox path="operatingSystems" value="ms window" />

        <br>
        <%--During form submission, spring will call setFirstName and setLastName--%>
        <input type="submit" value="Submit">
    </form:form>
</body>
</html>
package com.luv2code.springdemo.mvc;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;

public class Student {
    @Getter @Setter
    private String firstName;

    @Getter @Setter
    private String lastName;

    @Getter @Setter
    private String country;

    @Getter
    private LinkedHashMap<String, String> countryOptions;

    @Getter @Setter
    private String favouriteLanguage;

    @Getter
    private LinkedHashMap<String, String> favouriteLanguages;

    @Getter @Setter
    private String[] operatingSystems;

    public Student(){
        countryOptions = new LinkedHashMap<>();
        countryOptions.put("BR", "Brazil");
        countryOptions.put("FR", "France");
        countryOptions.put("DE", "Germany");
        countryOptions.put("IN", "India");
        countryOptions.put("US", "United states fof America");

        favouriteLanguages = new LinkedHashMap<>();
        favouriteLanguages.put("JAVA", "java");
        favouriteLanguages.put("C#", "csharp");
        favouriteLanguages.put("PHP", "php");
        favouriteLanguages.put("RUBY", "ruby");

    }

}

package com.luv2code.springdemo.mvc.controllers;

import com.luv2code.springdemo.mvc.Customer;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    // Add an @Initbinder... to convert trim input strings
    // remove leading and trailing whitespace
    // resolve issue for our validation
    // @InitBinder pre-processes every String form data
    // Remove leading and trailing white space
    // If String only has white space ... trim it to null.
    @InitBinder
    public void initBinder(WebDataBinder dataBinder){

        // 'true' parameter for StringTrimmerEditor() constructor mean 'trim to null'
        // (ie any leading and triailing white spaces)
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }

    @RequestMapping("/showForm")
    public String showForm(Model theModel){
        theModel.addAttribute("customer", new Customer());
        return "customer/customer-form";
    }

    // @Valid annotation - here performs validation for customer Objects.
    // @BindingResults annotation - Results of validation placed in the BindingResult.
    @RequestMapping("/processForm")
    public String processForm(@Valid @ModelAttribute("customer") Customer theCustomer,
                            BindingResult theBindingResult){

        System.out.println("last name: |" + theCustomer.getLastName() + "|");

        // inspecting bindingResult
        /*System.out.println("Binding result: " + theBindingResult + "\n");*/

        return theBindingResult.hasErrors() ? "customer/customer-form" : "customer/customer-confirmation";
    }
}

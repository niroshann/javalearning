package com.luv2code.springdemo.mvc;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

public class Customer {

    @Getter @Setter
    private String firstName;

    @NotNull(message="last name field is required")
    @Size(min=0, message="is required message from size annotation")
    @Getter @Setter
    private String lastName;

    @NotNull(message="free pass field is required")
    @Min(value=0, message="must be greater than or equal to zero")
    @Max(value=10, message="must be less than or equal to 10")
    @Getter @Setter
    private Integer freePasses;

    @Pattern(regexp = "^[a-zA-Z0-9]{5}", message="only 5 chars/digits")
    @Getter @Setter
    private String postalCode;


}
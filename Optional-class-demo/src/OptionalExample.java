import java.util.Optional;
import java.util.function.Function;

public class OptionalExample {
    public static void main(String[] args) {
        Function<String, String> getSecondWord = ( String s ) -> { return s.split( " ").length > 1 ? s.split(" ")[1] : null; };
        Function<String, Integer> getLetterCount = t -> t.length();

//        Integer count = getSecondWord.andThen(getLetterCount).apply("talha");
//        System.out.println(count);

        Optional<String> myOptional = Optional.ofNullable(getSecondWord.apply("talha"));
        boolean isPresent = myOptional.isPresent();

        if(myOptional.isPresent()) {
            //String s = myOptional.get();
            //System.out.println("our value: " + s);
            myOptional.ifPresent(System.out::println);
        } else {
            System.out.println(myOptional.orElse("no value"));
        }

        Optional.ofNullable(getSecondWord.apply("talha")).map(getLetterCount).ifPresent(System.out::println);
    }
}

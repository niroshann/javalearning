package lambda.learning.unit1;

public class Greeter {

    public static void main(String[] args) {

         Greeting helloWorldGreeting = new HelloWorldGreeting();
         Greeting myLambdaFunction = () -> System.out.println("printing through Lambda");

         // Anonymous class
        Greeting innerClassGreeting = new Greeting(){
            @Override
            public void perform() {
                System.out.println("Printing from inner anonymous class");
            }
        };

        // Lambda way
        myLambdaFunction.perform();

        // Traditional way
        helloWorldGreeting.perform();

        // Anonymous class way
        innerClassGreeting.perform();
    }
}



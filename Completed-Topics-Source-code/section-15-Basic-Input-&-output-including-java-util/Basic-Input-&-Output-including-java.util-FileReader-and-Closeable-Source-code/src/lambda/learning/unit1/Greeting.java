package lambda.learning.unit1;

@FunctionalInterface
interface Greeting{
    void perform();
}

package lambda.learning.unit1;

@FunctionalInterface
interface Condition{
    boolean test(Person p);
}

package lambda.learning.unit1;

public class TypeInferenceExample {

    public static void main(String[] args) {

     // Valid line
     //StringLengthLambda myLambda = (String s) -> s.length();

     // Again valid
     //StringLengthLambda myLambda = (s) -> s.length();

     // Again valid
     StringLengthLambda myLambda = (s) -> s.length();
     System.out.println(myLambda.getLenght("Hello Lambda"));

     // Calling lambda through methods another way
        //printLambda(String::length);

     // Calling lambda through methods preferred way
        printLambda(s -> s.length());

    }

    public static void printLambda(StringLengthLambda l){
        System.out.println(l.getLenght("Hello Lambda"));
    }
}


package lambda.learning.unit3;

import lambda.learning.unit1.Person;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class MethodReferenceExample2 {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("charles", "dickens", 60),
                new Person("lewis", "carroll", 42),
                new Person("thomas", "carlyle", 51),
                new Person("charlotte", "bronte", 45),
                new Person("Matthew", "Arnold", 39)
        );

        // Step 2: Create a method that prints all the elements in the list
        System.out.println("Printing all persons");

        // Structure you should be focusing is 'p -> method(p)'
        // performConditionally(people, p -> true, p -> System.out.println(p)); // Also true
        performConditionally(people, p -> true, System.out::println); // Also true

    }

    // Predicate is out of the box interfaces provided by java in package import java.util.function
    private static void performConditionally(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {

        for (Person p:
                people) {
            if(predicate.test(p)){
                consumer.accept(p);
            }
        }
        System.out.println("\n");
    }
}

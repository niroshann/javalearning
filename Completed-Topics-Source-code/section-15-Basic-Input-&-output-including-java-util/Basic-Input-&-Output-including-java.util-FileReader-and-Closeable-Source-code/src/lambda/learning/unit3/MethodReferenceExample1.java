package lambda.learning.unit3;

public class MethodReferenceExample1 {

    public static void main(String[] args) {


        //Thread t = new Thread(()-> printMessage());

        // This java line 'Thread t = new Thread(()-> printMessage());' can also be written as below
        // Basically its class name double colon and method name,
        // In below line class name is "MethodReferenceExample1" & method name is "printMessage"
        // This applies only when there isn't any 'Input arguements' & no 'Method arguments' such as '()-> printMessage()'
        // Therefore this is '()-> printMessage()' equivalent to 'MethodReferenceExample1::printMessage'
        // In other words the structure is '() -> method()'
        Thread t = new Thread(MethodReferenceExample1::printMessage);
        t.start();
    }

    public static void printMessage(){
        System.out.println("hello");
    }
}

package lambda.learning.unit2;

import lambda.learning.unit1.Person;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class StandardFunctionalnterfacesExample {

    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("charles", "dickens", 60),
                new Person("lewis", "carroll", 42),
                new Person("thomas", "carlyle", 51),
                new Person("charlotte", "bronte", 45),
                new Person("Matthew", "Arnold", 39)
        );

        // Step 1: Sort list by last name
        Collections.sort(people, (p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));

        // Step 2: Create a method that prints all the elements in the list
        System.out.println("Printing all persons");
        performConditionally(people, p -> true, p -> System.out.println(p));

        // Step 3: Create a method that prints all people that have last name beginning with C
        System.out.println("Printing all persons with last name beginning with C");
        performConditionally(people, (p1) -> p1.getLastName().startsWith("c"), p -> System.out.println(p));

        System.out.println("Printing all person with first name beginning with C");
        performConditionally(people, (Person p1) -> p1.getFirstName().startsWith("c"), p -> System.out.println(p.getFirstName()));
    }

    // Predicate is out of the box interfaces provided by java in package import java.util.function
    private static void performConditionally(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {

        for (Person p:
                people) {
            if(predicate.test(p)){
                consumer.accept(p);
            }
        }
        System.out.println("\n");
    }

}


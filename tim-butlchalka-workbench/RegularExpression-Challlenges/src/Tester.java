import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tester {
    public static void main(String[] args) {
//
//        String challenge1 = "I want a bike.";
//        String challenge2 = "I want a ball.";
//        System.out.println("challenge1 : " + challenge1.matches("^([A-Z]{1}[ ][a-z]{4}[ ][a-z]{1}[ ][a-z]{4}.)")); // 1st true
//        System.out.println("challenge2 : " + challenge2.matches("^([A-Z]{1}[ ][a-z]{4}[ ][a-z]{1}[ ][a-z]{4}.)")); // 1st true

        /**
         * Challenge: 2
         *
         * Write regular expression that will match "I want a bike." and "I want a ball"
         * Verify your answer using the matches() method.
         *
         * Match two strings beginning with "I want a "
         * */

        String challenge1 = "I want a bike.";
        String challenge2 = "I want a ball.";

        String regExp = "I want a \\w+.";
        System.out.println("challenge 1 with variable 'regExp' : " + challenge1.matches(regExp));
        System.out.println("challenge 2 with variable 'regExp' : " + challenge2.matches(regExp));

        // Grouping example for challenge: 2
        String regExp1 = "I want a (bike|ball).";
        System.out.println("challenge 1 with variable 'regExp1' : " + challenge1.matches(regExp1));
        System.out.println("challenge 2 with variable 'regExp1' : " + challenge2.matches(regExp1));

        /**
         * Challenge: 3
         *
         * Use Matcher.matches() method to check for matches, instead of String.matches(),
         * for the regular expression that uses \w+.
         * Hint: You'll have to compile a pattern
         *
         * */

        System.out.println("\nMatching through Matcher class \n===============");
        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(challenge1);
        System.out.println(matcher.matches());

        /**
         *
         * Challenge: 4
         *
         * Replace all occurrences of blank with an underscore for the following string.
         * Print out the resulting string.
         *
         * String challenge4 = "Replace all blanks with underscores.";
         * */

        String challenge4 = "Replace all blanks with underscores.";
        System.out.println("\nReplacing all the blanks with underscores\n===========" + challenge4.replaceAll("\\s", "_"));
        System.out.println("\nAnother way of replacing all the blanks with underscores\n===========" + challenge4.replaceAll(" ", "_"));

        /**
         *
         * Challenge: 5
         *
         * Write a regular expression that will match the following string in its entirety.
         * Use the String.matches() method to verify your answer.
         *
         * String challenge5 = "aaabccccccccdddefffg";
         * */

        String challenge5 = "aaabccccccccdddefffg";
        System.out.println("\nChallenge 5 list of ways to achieve regex: \n========================");
        System.out.println(challenge5.matches("^[abcdefg]+"));
        System.out.println(challenge5.matches("^[a-g]+"));
        System.out.println(challenge5.matches("^[a-z].*"));

        /**
         *
         * Challenge: 6
         *
         * Write a regular expression that will only match the challenge 5 string in its entirely
         * */

        System.out.println("\nChallenge 6: list of ways\n==================");
        //System.out.println(challenge5.matches("a{3}b{1}c{8}d{3}e{1}f{3}g{1}"));
        System.out.println(challenge5.matches("^a{3}bc{8}d{3}ef{3}g$"));
        System.out.println(challenge5.replaceAll("^a{3}bc{8}d{3}ef{3}g$", "REPLACED"));

        /**
         *
         * Challenge: 7
         *
         * */

        String challenge7 = "abcd.135";
        System.out.println("\nChallenge 7: list of ways\n==================");
        System.out.println(challenge7.matches("^[A-z][a-z]+\\.\\d+$"));
        System.out.println(challenge7.matches("^(?i)[a-z]+\\.\\d+$"));
        System.out.println(challenge7.matches("((?i)[a-z]+)(\\.\\d+)"));

        /**
         *
         * Challenge: 8
         *
         * */

        String challenge8 = "abcd.135uvqz.7tzik.999";
        System.out.println("\nChallenge 8: list of ways\n==================");
        //Pattern pattern8 = Pattern.compile("((?i)[a-z]+)(\\.\\d+(?i)[a-z]+)(\\.\\d(?i)[a-z]+)(\\.\\d+)"); // its true but not what asked in the challenge
        Pattern pattern8 = Pattern.compile("[A-Za-z]+\\.(\\d+)");
        Matcher matcher8 = pattern8.matcher(challenge8);
        //System.out.println(matcher8.matches());

        while(matcher8.find()) {
            System.out.println("Occurrence: " + matcher8.group(1));
        }

        /**
         *
         * Challenge: 9
         *
         * */
        String challenge9 = "abcd.135\tuvqz.7\ttzik.999\n";
        System.out.println("\nChallenge 9: list of ways\n==================");
        Pattern pattern9 = Pattern.compile("[A-Za-z]+\\.(\\d+)(\\s)");
        Matcher matcher9 = pattern9.matcher(challenge9);

        while(matcher9.find()) {
            System.out.println("Occurrence: " + matcher9.group(1));
        }

        /**
         *
         * Challenge: 10
         *
         * */
        String challenge10 = "abcd.135\tuvqz.7\ttzik.999\n";
        System.out.println("\nChallenge 10: list of ways\n==================");
        Pattern pattern10 = Pattern.compile("[A-Za-z]+\\.(\\d+)(\\s)");
        Matcher matcher10 = pattern10.matcher(challenge10);

        int count = 0;
        while(matcher10.find()) {
            count++;
            //String group = matcher10.group(2);
            System.out.println("Occurrence: " + count + " : value= " + matcher10.group(1) + " , " + matcher10.start(1) + " to " + (matcher10.end(1) -1));
        }

        /**
         * Challenge: 11
         * */

        String challenge11 = "{0,2}, {0,5}, {1,3}, {2,4}";
        //Pattern pattern11 = Pattern.compile("[^,{}\\s]"); // my shity answer for challenge 11
        Pattern pattern11 = Pattern.compile("\\{(.+?)\\}");
        Matcher matcher11 = pattern11.matcher(challenge11);

        System.out.println("\nChallenge 11: list of ways\n==================");
        while(matcher11.find()) {
            System.out.println("Occurrence: " + matcher11.group(1));
        }


        String challenge11a = "{0, 2}, {0, 5}, {1, 3}, {2, 4} {x, y}, {6, 34}, {11, 12}";
        Pattern pattern11a = Pattern.compile("\\{(\\d+, \\d+)\\}");
        Matcher matcher11a = pattern11a.matcher(challenge11a);

        System.out.println("\nChallenge 11a: list of ways\n==================");
        while(matcher11a.find()) {
            System.out.println("Occurrence: " + matcher11a.group(1));
        }

        /**
         * Challenge: 12
         * */

        String challenge12 = "12351";
        System.out.println("\nChallenge 12: list of ways\n==================");
        System.out.println(challenge12.matches("(\\d+){5}"));
        System.out.println(challenge12.matches("^\\d{5}$"));

        /**
         * Challenge: 13
         * */

        String challenge13 = "11111-1111";
        System.out.println("\nChallenge 13: list of ways\n==================");
        System.out.println(challenge13.matches("^\\d{5}-\\d{4}$"));

        /**
         * Challenge: 14
         * */

        // Optional strings after hyphen (-)
        String challenge14 = "11111-1111";
        System.out.println("\nChallenge 14: list of ways\n==================");
        System.out.println(challenge14.matches("^\\d{5}-(\\d{4})?$"));
    }
}

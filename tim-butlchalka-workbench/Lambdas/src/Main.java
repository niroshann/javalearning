import java.io.FilenameFilter;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        // One way of writing
        /*new Thread(new CodeToRun()).start();*/

        // Another way
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Printing from the Runable");
            }
        }).start();*/

        /*new Thread(() -> {
            System.out.println("Printing from the runnable in main");
            System.out.println("this is line 2");
            System.out.format("This is line %d\n", 3);
        }).start();*/

        Employee john = new Employee("John Doe", 30);
        Employee tim = new Employee("Tim Buchalka", 21);
        Employee jack = new Employee("Jack Hill", 40);
        Employee snow = new Employee("Snow White", 22);

        List<Employee> employees = new ArrayList<>(
                Arrays.asList(john,tim,jack,snow));

        // Java 7 way of doing things.
        /*Collections.sort(employees, new Comparator<Employee>() {
            @Override
            public int compare(Employee employee1, Employee employee2) {
                return employee1.getName().compareTo(employee2.getName());
            }
        });*/

        // short lambda way of doing this < Collections.sort(employees, (employee1, employee2) -> employee1.getName().compareTo(employee2.getName())); >
        /*Collections.sort(employees, Comparator.comparing(Employee::getName));*/

        /*Collections.sort(employees, (employee1, employee2) -> employee1.getName().compareTo(employee2.getName()));
        employees.forEach(employee -> System.out.println("Employee name is : " + employee.getName()));*/

        // lambda assigning to a variable
        /*UpperConcat up = (s1, s2) -> s1.toUpperCase() + s2.toUpperCase();*/

        // One liner
        /*String sillyString = doStringStuff((s1, s2) -> s1.toUpperCase() + s2.toUpperCase(), employees.get(0).getName(), employees.get(1).getName());
        System.out.println(sillyString);*/

        /*AnotherClass anotherClass = new AnotherClass();
        System.out.println(anotherClass.doSomething());*/

        employees.forEach(employee -> {
            System.out.println(employee.getName());
            System.out.println(employee.getAge());
        });
    }

    public final static String doStringStuff(UpperConcat uc, String s1, String s2){
        return uc.upperAndConcat(s1, s2);
    }
}

class Employee{
    private String name;
    private int age;

    public Employee(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

class CodeToRun implements Runnable {

    @Override
    public void run() {
        System.out.println("Printing from runnable");
    }
}

interface UpperConcat{
    public String upperAndConcat(String s1, String s2);
}

class AnotherClass {
    public String doSomething(){

        int i = 0;

        System.out.println("The AnotherClass class's name is: " + getClass().getSimpleName());

        /*return Main.doStringStuff(new UpperConcat() {
            @Override
            public String upperAndConcat(String s1, String s2) {
                System.out.println("The Anonymous class's name is: " + getClass().getSimpleName());
                return s1.toUpperCase() + s2.toUpperCase();
            }
        },"String1", "String2");*/

        // Lambda way for the above anonymous class
        return Main.doStringStuff((s1, s2) -> {
            System.out.println("The lambda expression class's name is " + getClass().getSimpleName());
            System.out.println("i in lambda expression = " + i);
            return s1.toUpperCase() + s2.toUpperCase();
        }, "String1", "String2");
    }

    public void printValue(){
        int number = 25;

        Runnable r = () -> {
          try{
              Thread.sleep(2000);
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
            System.out.println("THe value is " + number);
        };

        new Thread(r).start();
    }
}
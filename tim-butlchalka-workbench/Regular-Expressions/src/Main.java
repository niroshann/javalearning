import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {

        /**
         *
         * General notes
         * '()' Parenthesis for grouping.
         * '.' "DOT IS CHARACTER CLASS" matches any character, (ie) basically a wild card for any character
         * */

        String string = "I am a string. Yes, I am.";
        String yourString = string.replaceAll("I", "You");
        System.out.println("Replacing 'I' with 'You' in 'string' variable contents\n============\n" +
                "actual 'string' variable content => " + string + "\n'yourString' variable content => " + yourString + "\n");


        String alphanumeric = "abcDeeeF12Ghhiiiijkl99z";

        // '.' "DOT IS CHARACTER CLASS" matches any character, (ie) basically a wild card for any character
        System.out.println("\nReplacing 'Y' with all characters in 'alphanumeric' variable contents\n============\n" +
                "actual 'alphanumeric' variable content => " + alphanumeric + "\nmodified 'alphanumeric' variable content => " + alphanumeric.replaceAll(".","Y")+ "\n");


        //"^CARROT BOUNDARY MARCHER" so it's basically a wild card for any matcher that's located doing a shift 6 character now the carrot boundary.
        //that is always followed by pattern which matcher, it could be a string literal or something that is always followed by pattern which
        //is more complex when we use this carrot could be a string literal or something
        System.out.println("\nReplacing 'abcDeee' with 'YYY' characters in 'alphanumeric' variable contents\n============\n" +
                "actual 'alphanumeric' variable content => " + alphanumeric + "\nmodified 'alphanumeric' variable content => " + alphanumeric.replaceAll("^abcDeee", "YYY")+ "\n");


        // This is an example, when carrot wild card is used, it only looks up for the beginning of string with the given pattern.
        // In variable 'secondString' there're two occurrences of 'abcDeee' however once processed it only replaces the first occurrence.
        String secondString = "abcDeeeF12GhhabcDeeeiiiijkl99z";
        System.out.println("\nReplacing 'abcDeee' with 'YYY' characters in 'secondString' variable contents\n============\n" +
                "actual 'secondString' variable content => " + secondString + "\nmodified 'secondString' variable content => " + secondString.replaceAll("^abcDeee", "YYY")+ "\n");


        // matches() method expects the entire regex to pass otherwise it'll return false
        System.out.println(alphanumeric.matches("^hello")); // will be false
        System.out.println(alphanumeric.matches("^abcDeee")); // will be false
        System.out.println(alphanumeric.matches("^abcDeeeF12Ghhiiiijkl99z") + "\n"); // will true

        // '$' "DOLLAR SYMBOL BOUNDARY MATCHER" actually match strings that end with the preceded by a pattern. (ie) Nothing happens after the '$' symbol.
        // (ie) Its the opposite of '^CARROT BOUNDARY MATCHER"
        System.out.println("\nReplacing 'ijkl99z' with 'THE END' characters in 'alphanumeric' variable contents\n============\n" +
                "actual 'alphanumeric' variable content => " + alphanumeric + "\nmodified 'alphanumeric' variable content => " + alphanumeric.replaceAll("ijkl99z$", "THE END")+ "\n");


        // Find and replace all occurrence of "aei" with "X"
        System.out.println("\nReplacing all occurrance of 'aei' with 'X' characters in 'alphanumeric' variable contents\n============\n" +
                "actual 'alphanumeric' variable content => " + alphanumeric + "\nmodified 'alphanumeric' variable content => " + alphanumeric.replaceAll("[aei]", "X")+ "\n");


        // Find and replace all occurrences of "aei" followed by an "F" or "j" with "X"
        System.out.println("\nReplace occurrences of 'aei' followed by an 'F' or 'j' with 'X' characters in 'alphanumeric' variable contents\n============\n" +
                "actual 'alphanumeric' variable content => " + alphanumeric + "\nmodified 'alphanumeric' variable content => " + alphanumeric.replaceAll("[aei][Fj]", "X")+ "\n");

        String newAlphanumeric = "abcDeeeF12Ghhiiiijkl99z";

        // When ^CARROT is used within square brackets its CHARACTER CLASS not a BOUNDARY MATCHER
        // In below example it negates the regex mentioned in the square brackets
        // (ie) it replaces all the characters with 'X' apart from "ej"
        System.out.println("\nReplace all characters of 'newAlphanumeric' variable contents apart from characters 'e' and 'j'\n============\n" +
                "actual 'newAlphanumeric' variable content => " + newAlphanumeric + "\nmodified 'newAlphanumeric' variable content => " + newAlphanumeric.replaceAll("[^ej]", "X")+ "\n");


        // Regular expressions are case sensitive, below is an example
        // (ie) characters 'D' and 'F' aren't replaced.
        System.out.println("\nReplacing all occurrence of 'abcdef345678' with 'X' characters in 'newAlphanumeric' variable contents\n============\n" +
                "actual 'newAlphanumeric' variable content => " + newAlphanumeric + "\nmodified 'newAlphanumeric' variable content => " + newAlphanumeric.replaceAll("[abcdef345678]", "X")+ "\n");


        // Another case sensitive example
        // (ie) characters 'D' and 'F' aren't replaced.
        System.out.println("\nReplacing all occurrence of 'abcdef345678' with 'X' characters in 'newAlphanumeric' variable contents\n============\n" +
                "actual 'newAlphanumeric' variable content => " + newAlphanumeric + "\nmodified 'newAlphanumeric' variable content => " + newAlphanumeric.replaceAll("[a-f3-8]", "X")+ "\n");


        //Another case sensitive example
        // (ie) characters 'D' and 'F' are replaced.
        System.out.println("\nReplacing all occurrence of 'abcdef345678' with 'X' characters in 'newAlphanumeric' variable contents\n============\n" +
                "actual 'newAlphanumeric' variable content => " + newAlphanumeric + "\nmodified 'newAlphanumeric' variable content => " + newAlphanumeric.replaceAll("[a-fA-F3-8]", "X")+ "\n");


        //Another case sensitive example
        // Parenthesis here for grouping.
        // (ie) characters 'D' and 'F' are replaced.
        // special construct for case sensitivity "(?i)"
        // "(?i)" works for ASCII code however for unicode use 'u' at the end like this "(?iu)"
        System.out.println("\nReplacing all occurrence of 'abcdef345678' with 'X' characters in 'newAlphanumeric' variable contents\n============\n" +
                "actual 'newAlphanumeric' variable content => " + newAlphanumeric + "\nmodified 'newAlphanumeric' variable content => " + newAlphanumeric.replaceAll("(?i)[a-f3-8]", "X")+ "\n");


        // Replacing digits example
        System.out.println("\nReplacing all occurrence of '0-9' with 'X' characters in 'newAlphanumeric' variable contents\n============\n" +
                "actual 'newAlphanumeric' variable content => " + newAlphanumeric + "\nmodified 'newAlphanumeric' variable content => " + newAlphanumeric.replaceAll("[0-9]", "X")+ "\n");


        // Another way replacing digits
        // '//' two back slashes here for skip-ing the 1st back slash
        System.out.println("\nReplacing all occurrence of '0-9' with 'X' characters in 'newAlphanumeric' variable contents\n============\n" +
                "actual 'newAlphanumeric' variable content => " + newAlphanumeric + "\nmodified 'newAlphanumeric' variable content => " + newAlphanumeric.replaceAll("\\d", "X")+ "\n");


        // Another way replacing all NON DIGITS
        System.out.println("\nReplacing all occurrence of NON DIGITS with 'X' characters in 'newAlphanumeric' variable contents\n============\n" +
                "actual 'newAlphanumeric' variable content => " + newAlphanumeric + "\nmodified 'newAlphanumeric' variable content => " + newAlphanumeric.replaceAll("\\D", "X")+ "\n");


        String hasWhitespace = "I have blanks and\ta tab, and also a newline\n";

        System.out.println("\nReplacing all occurrence of SPACES & TABS with empty space characters in 'hasWhitespace' variable contents\n============\n" +
                "actual 'hasWhitespace' variable content => " + hasWhitespace + "\nmodified 'hasWhitespace' variable content => " + hasWhitespace.replaceAll("\\s", "")+ "\n");


        System.out.println("\nReplacing all occurrence of TABS with 'X' characters in 'hasWhitespace' variable contents\n============\n" +
                "actual 'hasWhitespace' variable content => " + hasWhitespace + "\nmodified 'hasWhitespace' variable content => " + hasWhitespace.replaceAll("\t", "X")+ "\n");


        System.out.println("\nReplacing all occurrence of NON 'TABS' and 'SPACES' with empty characters in 'hasWhitespace' variable contents\n============\n" +
                "actual 'hasWhitespace' variable content => " + hasWhitespace + "\nmodified 'hasWhitespace' variable content => " + hasWhitespace.replaceAll("\\S", "")+ "\n");

        // Character class '\\w' class matches 'a-z' and 'A-Z' and '0-9' and underscore '_'
        // And Character class '\\W' will have the opposite effect
        System.out.println("\nReplacing all occurrence with 'X' characters in 'newAlphanumeric' variable contents\n============\n" +
                "actual 'newAlphanumeric' variable content => " + newAlphanumeric + "\nmodified 'newAlphanumeric' variable content => " + newAlphanumeric.replaceAll("\\w", "X")+ "\n");


        // Character class '\\w' class matches 'a-z' and 'A-Z' and '0-9' and underscore '_'
        // And Character class '\\W' will have the opposite effect
        System.out.println("\nReplacing all occurrence with 'X' apart from TAB and SPACE characters in 'hasWhitespace' variable contents\n============\n" +
                "actual 'hasWhitespace' variable content => " + hasWhitespace + "\nmodified 'hasWhitespace' variable content => " + hasWhitespace.replaceAll("\\W", "X")+ "\n");

        System.out.println("\nReplacing all occurrence with 'X' characters in 'newAlphanumeric' variable contents\n============\n" +
                "actual 'newAlphanumeric' variable content => " + newAlphanumeric + "\nmodified 'newAlphanumeric' variable content => " + newAlphanumeric.replaceAll("\\W", "X")+ "\n");


        // Example for surrounding characters at the beginning and end of a word using character class '\\b'
        System.out.println("\nReplacing all occurrence with 'X' apart from TAB and SPACE characters in 'hasWhitespace' variable contents\n============\n" +
                "actual 'hasWhitespace' variable content => " + hasWhitespace + "\nmodified 'hasWhitespace' variable content => " + hasWhitespace.replaceAll("\\b", "X")+ "\n");


        /**
         *
         * Quantifiers specify, how often a regular expression occurs in a given string.
         * */

        String thirdAlphanumericString = "abcDeeeF12Ghhiiiijkl99z";

        // so the three in curly braces indicates the number of preceding character that must occur in order for there to be a match
        // Replacing 'abcDeee' followed by one or more 'e'
        System.out.println("\nReplacing 'abcDeee' with 'YYY' characters in 'thirdAlphanumericString' variable contents\n============\n" +
                "actual 'thirdAlphanumericString' variable content => " + thirdAlphanumericString + "\nmodified 'thirdAlphanumericString' variable content => " + thirdAlphanumericString.replaceAll("^abcDe{3}", "YYY")+ "\n");


        // Another way of Replacing 'abcDeee' followed by one or more 'e'
        System.out.println("\nReplacing 'abcDeee' with 'YYY' characters in 'thirdAlphanumericString' variable contents\n============\n" +
                "actual 'thirdAlphanumericString' variable content => " + thirdAlphanumericString + "\nmodified 'thirdAlphanumericString' variable content => " + thirdAlphanumericString.replaceAll("^abcDe+", "YYY")+ "\n");


        // Another way of Replacing 'abcDeee' followed by one or more 'e'
        // THis method is bit more effective because it will replace even if the string changes to 'abcDe'
        System.out.println("\nReplacing 'abcDeee' with 'YYY' characters in 'thirdAlphanumericString' variable contents\n============\n" +
                "actual 'thirdAlphanumericString' variable content => " + thirdAlphanumericString + "\nmodified 'thirdAlphanumericString' variable content => " + thirdAlphanumericString.replaceAll("^abcDe*", "YYY")+ "\n");


        // Another way of Replacing 'abcDeee' followed by one or more 'e'
        // Occurrences of 2-5 'e' in a given pattern
        System.out.println("\nReplacing 'abcDeee' with 'YYY' characters in 'thirdAlphanumericString' variable contents\n============\n" +
                "actual 'thirdAlphanumericString' variable content => " + thirdAlphanumericString + "\nmodified 'thirdAlphanumericString' variable content => " + thirdAlphanumericString.replaceAll("^abcDe{2,5}", "YYY")+ "\n");

        // replacing all occurrences of 'h' followed by any number of 'i's followed by at least one 'j' with a 'y'
        System.out.println("\nReplacing all occurrences of 'h' followed by any number of 'i's followed by at least one 'j' with a 'y' characters in 'thirdAlphanumericString' variable contents\n============\n" +
                "actual 'thirdAlphanumericString' variable content => " + thirdAlphanumericString + "\nmodified 'thirdAlphanumericString' variable content => " + thirdAlphanumericString.replaceAll("h+i*j", "Y")+ "\n");


        StringBuilder htmlText = new StringBuilder("<h1>My Heading</h1>");
        htmlText.append("<h2>Sub-heading</h2>");
        htmlText.append("<p>This is a paragraph about something.</p>");
        htmlText.append("<p>This is another paragraph about something else.</p>");
        htmlText.append("<h2>Summary</h2>");
        htmlText.append("<p>Here is the summary.</p>");

        String h2Pattern = ".*<h2>.*";
        Pattern pattern = Pattern.compile(h2Pattern);
        Matcher matcher = pattern.matcher(htmlText);
        System.out.println(matcher.matches());

        matcher.reset();
        int count = 0;
        while(matcher.find()) {
            count++;
            System.out.println("Occurrence " + count + " : " + matcher.start() + " to " + matcher.end());

        }


        // GREEDY QUANTIFIER EXAMPLE
        // What we saying here, get opening tag and closing tag and everything inbetween.
        // '.' "DOT IS CHARACTER CLASS" matches any character, (ie) basically a wild card for any character
        // '*' "* QUANTIFIER" means 0 or more, this quantifier is also known as greedy quantifiers.
        // (because in our example which is 'htmlText' string, there are more occurrences even after the first "</h2>" tag.)

        String h2GroupPattern = "(<h2>.*</h2>)";
        Pattern groupPattern = Pattern.compile(h2GroupPattern);
        Matcher groupMatcher = groupPattern.matcher(htmlText);
        System.out.println(groupMatcher.matches());
        groupMatcher.reset();

        System.out.println("\nGreedy quantifier example\n====================");
        while(groupMatcher.find()) {
            System.out.println("Occurrence: " + groupMatcher.group(1));
        }

        // LAZY QUANTIFIER EXAMPLE
        // '?' "? LAZY QUANTIFIER" which is a lazy quantifier
        // lazy quantifier one that does the minimum amount of work so once it finds a match it doesn't keep
        // looking to see if it can include more characters in the match so it may still
        // find more matches and later parts of the character sequence but each match will contain the minimum number of characters
        // required for match.
        // Now the "?" quantifier means one or zero occurrences is a lazy quantifier so we can use it to turn a greedy quantifier into a lazy quantifier by actually adding it after the

        String h2GroupPattern1 = "(<h2>.*?</h2>)";
        Pattern groupPattern1 = Pattern.compile(h2GroupPattern1);
        Matcher groupMatcher1 = groupPattern1.matcher(htmlText);
        System.out.println(groupMatcher1.matches());
        groupMatcher1.reset();

        System.out.println("\nLazy quantifier example\n====================");
        while(groupMatcher1.find()) {
            System.out.println("Occurrence: " + groupMatcher1.group(1));
        }

        // LAZY QUANTIFIER loading different groups
        // In below (<h2>)(.+?)(</h2>)
        // (<h2>) -> group1
        // (.+?) -> group2
        // (</h2>) -> group3
        String h2TextGroups = "(<h2>)(.+?)(</h2>)";
        Pattern h2TextPatten = Pattern.compile(h2TextGroups);
        Matcher h2TextMatcher = h2TextPatten.matcher(htmlText);

        System.out.println("\nLazy quantifier example loading different groups\n====================");
        while(h2TextMatcher.find()) {
            System.out.println("Occurrence: " + h2TextMatcher.group(2));
        }

        // AND examples
        // In the lectures AND is already covered,
        // (ie) when a regex is used for "abc" "a" and "b" and "c"

        // OR examples
        System.out.println("\n'Or' Examples\n=================");
        System.out.println("harry".replaceAll("[Hh]arry", "Larry"));
        System.out.println("harry".replaceAll("[H|h]arry", "Larry"));
        System.out.println("Harry".replaceAll("[Hh]arry","Larry"));
        System.out.println("harry".replaceAll("[Hh]arry", "Larry"));


        // NOT examples
        // [^abc] means all characters apart from "abc"
        // '!' "EXCLAMATION MARK NOT OPERATOR IN NEGATIVE LOOKAHED CHARACTER"

        // Occurrences of 't' which are not followed by 'v'
        String tvTest = "tstvtkt";

        // Below regex will find 't's that are NOT followed by 'v', (ie) it will miss the last 't' in the variable 'tvTest'
        /*String tNotVRegExp = "t[^v]";*/

        // Below is much better approach
        // In below '(?' left parenthesis and question mark are part of NEGATIVE SYNTAX
        // '!' "EXCLAMATION MARK 'NOT' OPERATOR IN NEGATIVE LOOKAHED CHARACTER"
        // '=' "EQUAL TO SYMBOL IS THE OPPOSITE OF '!' WHICH WILL LOOKAHED FOR POSITIVE CHARACTERS"

        String tNotVRegExp = "t(?!v)";

        Pattern tNotVPattern = Pattern.compile(tNotVRegExp);
        Matcher tNotVMatcher = tNotVPattern.matcher(tvTest);

        System.out.println("\nOccurrences of 't' which are not followed by 'v'\n===================");
        count = 0;
        while (tNotVMatcher.find()){
            count++;
            System.out.println("Occurrence " + count + " : " + tNotVMatcher.start() + " to " + tNotVMatcher.end());
        }

        // Regex for sample US phone number
        // sample US phone number (800)123-4567
        /*  ^([\(]{1}[0-9]{3}[\)]{1}[ ]{1}[0-9]{3}[\-]{1}[0-9]{4})$  */

        String phone1 = "1234567890";  // Shouldn't match
        String phone2 = "(123) 456-7890"; // match
        String phone3 = "123 456-7890"; // Shouldn't match
        String phone4 = "(123)456-7890"; // Shouldn't match

        System.out.println("\nUs telephone number regex examples\n=======================");
        System.out.println("phone1 = " + phone1.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));
        System.out.println("phone2 = " + phone2.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));
        System.out.println("phone3 = " + phone3.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));
        System.out.println("phone4 = " + phone4.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));


        // ^4[0-9]{12}([0-9]{3})?$
        String visa1 = "4444444444444"; // should match
        String visa2 = "5444444444444"; // shouldn't match
        String visa3 = "4444444444444444";  // should match
        String visa4 = "4444";  // shouldn't match


        System.out.println("\nUs visa number regex examples\n=======================");
        System.out.println("visa1 " + visa1.matches("^4[0-9]{12}([0-9]{3})?$"));
        System.out.println("visa2 " + visa2.matches("^4[0-9]{12}([0-9]{3})?$"));
        System.out.println("visa3 " + visa3.matches("^4[0-9]{12}([0-9]{3})?$"));
        System.out.println("visa4 " + visa4.matches("^4[0-9]{12}([0-9]{3})?$"));
    }
}

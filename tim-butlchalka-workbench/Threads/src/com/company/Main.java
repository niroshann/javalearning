package com.company;

import static com.company.ThreadColor.ANSI_GREEN;
import static com.company.ThreadColor.ANSI_PURPLE;
import static com.company.ThreadColor.ANSI_RED;

public class Main {
    public static void main(String[] args) {
        System.out.println(ANSI_PURPLE + "Hello from the main thread");

        Thread anotherThread = new AnotherThread();
        anotherThread.setName("== Another Thread ==");
        anotherThread.start();

        // Another way to create thread using annoymous class
        /*new Thread(){
            public void run(){
                System.out.println("Hello from the anonymous class thread");
            }
        }.start();*/

        new Thread(() -> System.out.println(ANSI_GREEN + "Hello from the anonymous class thread")).start();

        // Calling MyRunable class through Thread class
        /*Thread myRunableThread = new Thread(new MyRunnable() {
            @Override
            public void run(){
                super.run();
            }
        });*/

        Thread myRunableThread;
        myRunableThread = new Thread(new MyRunnable() {
            @Override
            public void run(){
                System.out.println(ANSI_RED + "Hello anonymous class's implementation of run()");
                try{
                    anotherThread.join();
                    System.out.println(ANSI_RED + "Another thread terminated, or timed out, so i'm running");
                } catch (InterruptedException e){
                    System.out.println(ANSI_RED + "I couldn't wait after all. I was interrupted");
                }
            }
        });

        myRunableThread.start();
        //anotherThread.interrupt();

        System.out.println(ANSI_PURPLE + "Hello again from the main thread");

        // This will throw an exception, "IllegalThreadStateException", therefore not advisable.
        //anotherThread.start();
    }
}

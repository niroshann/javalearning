package com.company;

public class Main {

    public static void main(String[] args) {
        BankAccountUpToChallenge6 account = new BankAccountUpToChallenge6("12345-678", 1000);

        Thread trThread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                account.deposit(300);
                account.withDraw(50);
                account.printBalance();
            }
        });

        Thread trThread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                account.deposit(203.75);
                account.withDraw(100);
                account.printBalance();
            }
        });

        trThread1.start();
        trThread2.start();
    }
}

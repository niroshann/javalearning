package com.company;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class BankAccountUpToChallenge6 {
    private double balance;
    private String accountNumber;
    private ReentrantLock bufferLock;

    public BankAccountUpToChallenge6(String accountNumber, double initialBalance) {
        this.accountNumber = accountNumber;
        this.balance = initialBalance;
        this.bufferLock = new ReentrantLock();
    }

    public double getBalance() {
        return balance;
    }

    // Synchronized method example for deposit & withDraw
    /*
    public void deposit(double amount){
        synchronized (this){
            balance += amount;
        }
    }

    public void withDraw(double amount){
        synchronized (this){
            balance -= amount;
        }
    }*/

    // Lock() method example for deposit & withdraw
    /*public void deposit(double amount){
        bufferLock.lock();
        try{
            System.out.println("Depositing ... through " + Thread.currentThread().getName());
            balance += amount;
        } finally {
            bufferLock.unlock();
        }
    }

    public void withDraw(double amount){

        bufferLock.lock();
        try{
            System.out.println("Withdrawing ... through " + Thread.currentThread().getName());
            balance -= amount;
        } finally {
            bufferLock.unlock();
        }
    }*/

    public void deposit(double amount){

        boolean status = false;
        try {
            if(bufferLock.tryLock(1, TimeUnit.SECONDS)){
                try{
                    System.out.println("Depositing ... through " + Thread.currentThread().getName());
                    balance += amount;
                    status = true;
                } finally {
                    bufferLock.unlock();
                }
            } else {
                System.out.println("couldn't get the lock");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Transaction deposit() status = " + status + Thread.currentThread().getName());
    }

    public void withDraw(double amount){


        boolean status = false;
        try {
            if(bufferLock.tryLock(1, TimeUnit.SECONDS)){
                try{
                    System.out.println("Withdrawing ... through " + Thread.currentThread().getName());
                    balance -= amount;
                    status = true;
                } finally {
                    bufferLock.unlock();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Transaction withDraw() status = " + status + Thread.currentThread().getName());

    }

    public String getAccountNumber(){
        return accountNumber;
    }

    public void printAccountNumber(){
        System.out.println("Account Number: " + accountNumber);
    }

    public void printBalance(){
        System.out.println("Current Balance: " + getBalance() + " end of thread "+ Thread.currentThread().getName());
    }
}

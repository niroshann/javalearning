package com.company;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        /*Runnable runnable = () -> {
            String myString = "Let's split this up into an array";
            String[] parts = myString.split(" ");
            for (String part : parts) {
                System.out.println(part);
            }
        };
        runnable.run();*/

        //
        Function<String, String> charToChange = source -> {
            StringBuilder returnVal = new StringBuilder();
            for (int i = 0; i < source.length(); i++) {
                if (i % 2 == 1) {
                    returnVal.append(source.charAt(i));
                }
            }
            return returnVal.toString();
        };

        // Challenge 4
        /*System.out.println(everySecondCharacter(s -> {

            StringBuilder returnVal = new StringBuilder();
            for (int i = 0; i < s.length(); i++) {
                if (i % 2 == 1){
                    returnVal.append(s.charAt(i));
                }
            }
            return returnVal.toString();
        }, "1234567890"));*/

        System.out.println(everySecondCharacter(charToChange, "1234567890"));

        // Challenge 6
        // Example of calling supplier directly
        Supplier<String> stringSupplier = () -> "printing string through supplier";

        String supplierResult = stringSupplier.get();
        System.out.println(supplierResult);

        // Challenge 9
        List<String> topNames2015 = Arrays.asList(
                "Amelia",
                "Olivia",
                "emily",
                "Isla",
                "Ava",
                "oliver",
                "Jack",
                "Charlie",
                "harry",
                "Jacob"
        );


        // Challenge 9
        /*topNames2015
                .stream()
                .map(name -> name.substring(0, 1).toUpperCase() + name.substring(1))
                .sorted(Comparator.naturalOrder())
                .forEach(System.out::println);*/

        // challenge 12
        topNames2015
                .stream()
                .map(name -> name.substring(0,1).toUpperCase() + name.substring(1))
                .filter(name -> name.startsWith("A"))
                .sorted(Comparator.naturalOrder())
                .forEach(System.out::println);
    }

    private static String everySecondCharacter(Function<String, String> charToChange, String source) {
        return charToChange.apply(source);
    }
}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.*;

public class Main {

    public static void main(String[] args) {

        Employee john = new Employee("John Doe", 30);
        Employee tim = new Employee("Tim Buchalka", 21);
        Employee jack = new Employee("Jack Hill", 40);
        Employee snow = new Employee("Snow White", 22);
        Employee red = new Employee("Red RidingHood", 35);
        Employee charming = new Employee("Prince Charming", 31);

        List<Employee> employees = new ArrayList<>(
                Arrays.asList(john,tim,jack,snow,red, charming));

        /*employees.forEach(employee -> {
            System.out.println(employee.getName());
            System.out.println(employee.getAge());
        });*/

        //System.out.println("Employees over 30:\n====================== ");

        // enchanced for loop for Employees over 30:
        /*for (Employee employee: employees) {
            if(employee.getAge() > 30){
                System.out.println(employee.getName());
            }
        }*/

        // lambda forEach way Employees over 30:
        /*employees.forEach(employee -> {
            if(employee.getAge() > 30){
                System.out.println(employee.getName());
            }
        });

        System.out.println("\n Employees younger 30:\n====================== ");
        employees.forEach(employee -> {
            if(employee.getAge() <= 30){
                System.out.println(employee.getName());
            }
        });*/

        printEmployessByAge(employees, "\nEmployees over 30", employee -> employee.getAge() > 30);
        printEmployessByAge(employees, "\nEmployees 30 and under ", employee -> employee.getAge() <= 30);

        IntPredicate greaterThan15 = i -> i > 15;
        IntPredicate lessThan100 = i -> i < 100;

        System.out.println(greaterThan15.test(10));
        int a = 20;
        System.out.println(greaterThan15.test(a + 5));
        System.out.println(greaterThan15.and(lessThan100).test(50));
        System.out.println(greaterThan15.and(lessThan100).test(15));


        // supplier example
        /*Random random = new Random();
        Supplier<Integer> randomSupplier = () -> random.nextInt(1000);

        for (Integer i = 0; i < 10; i++) {
            System.out.println(randomSupplier.get());
        }*/

        /*employees.forEach(employee -> {
            String lastName = employee.getName().substring(employee.getName().indexOf(' ') + 1);
            System.out.println("Last name is: " + lastName);
        });*/

        Function<Employee, String> getLastName =
                (Employee employee) ->
                employee.getName().substring(employee.getName().indexOf(' ') + 1);

        String lastName = getLastName.apply(employees.get(1));
        System.out.println("lastName :" + lastName);

        Function<Employee, String> getFirstName = (Employee employee) ->{
           return employee.getName().substring(0, employee.getName().indexOf(' '));
        };


        Random random = new Random();
        for (Employee employee : employees) {
            if(random.nextBoolean()) {
                System.out.println("firstName from getAName(): " + getAName(getFirstName, employee));
            } else {
                System.out.println("lastName from getAName(): " + getAName(getLastName, employee));
            }
        }

        Function<Employee, String> upperCase = employee -> employee.getName().toUpperCase();
        Function<String, String> firstName = name -> name.substring(0, name.indexOf(' '));
        Function chainedFunction = upperCase.andThen(firstName);
        System.out.println(chainedFunction.apply(employees.get(0)));

        BiFunction<String, Employee, String> concatAge = (String name, Employee employee) -> {
            return name.concat(" " + employee.getAge());
        };

        String upperName = upperCase.apply(employees.get(0));
        System.out.println(concatAge.apply(upperName, employees.get(0)));

        IntUnaryOperator incBy5 = i -> i + 5;
        System.out.println(incBy5.applyAsInt(10));

        Consumer<String> c1 = s -> s.toUpperCase();
        Consumer<String> c2 = s -> System.out.println(s);
        c1.andThen(c2).accept("Hello, World");
    }

    public static String getAName(Function<Employee, String> getName, Employee employee){
        return getName.apply(employee);
    }

    public static void printEmployessByAge(List<Employee> employees,
                                           String ageText,
                                           Predicate<Employee> ageCondition){

        System.out.println(ageText + "\n==================");
        for (Employee employee:
             employees) {
            if(ageCondition.test(employee)){
                System.out.println(employee.getName());
            }
        }
    }
}

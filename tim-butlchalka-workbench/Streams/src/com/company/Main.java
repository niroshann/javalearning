package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        List<String> someBingoNumbers = Arrays.asList(
            "N40", "N36",
            "B12", "B6",
            "G53", "G49", "G60", "G50", "g64",
            "I26", "I17", "I29",
            "O71");

        List<String> gNumbers = new ArrayList<>();

        someBingoNumbers.forEach(number -> {
            if(number.toUpperCase().startsWith("G")){
                gNumbers.add(number);
                //System.out.println(number);
            }
        });

        // Sorting example
        //gNumbers.sort(Comparator.naturalOrder());

        // Another sorting example
        System.out.println("Printing G/g numbers in non-stream way\n===============");
        gNumbers.sort((s1, s2) -> s1.compareTo(s2));
        gNumbers.forEach(s -> System.out.println(s));


        System.out.println("\nPrinting G/g numbers in stream way\n===============");
        someBingoNumbers.stream()
                .map(String::toUpperCase)
                .filter(s -> s.startsWith("G"))
                .sorted()
                .forEach(System.out::println);

        // Example of creating streams
        Stream<String> ioNumberStream = Stream.of("I26","I17","I29","I71");
        Stream<String> inNumberStream = Stream.of("N40","N36","I26","I17","I29","O71");

        // Stream concatination example
        Stream<String> concatStream = Stream.concat(ioNumberStream, inNumberStream);
        System.out.println("\n----------------------------\n");
        System.out.println(concatStream
                        .distinct()
                        .peek(System.out::println)
                        .count());


        // Flatmap & Lambda best practices
        Employee john = new Employee("John Doe", 30);
        Employee jane = new Employee("Jack Deer", 25);
        Employee jack = new Employee("Jack Hill", 40);
        Employee snow = new Employee("Snow White", 22);

        Department hr = new Department("Human Resources");
        hr.addEmployee(jane);
        hr.addEmployee(jack);
        hr.addEmployee(snow);

        Department accounting = new Department("Human Resources");
        accounting.addEmployee(john);

        List<Department> departments = new ArrayList<>();
        departments.add(hr);
        departments.add(accounting);

        departments
                .stream()
                .flatMap(department -> department.getEmployees().stream())
                .forEach(System.out::println);

        System.out.println("\nSorted GNumbers from someBingoNumbersList\n==============");

        // Collecting as a List example
        /*List<String> sortedGNumbers = someBingoNumbers
                .stream()
                .map(String::toUpperCase)
                .filter(s -> s.startsWith("G"))
                .sorted()
                .collect(Collectors.toList());*/

        // Collecting as ArrayList
        List<String> sortedGNumbers = someBingoNumbers
                .stream()
                .map(String::toUpperCase)
                .filter(s -> s.startsWith("G"))
                .sorted()
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);

        sortedGNumbers.forEach(System.out::println);

        // Grouping by employees age
        Map<Integer, List<Employee>> groupedByAge = departments.stream()
                .flatMap(department -> department.getEmployees().stream())
                .collect(Collectors.groupingBy(Employee::getAge));


        // Reduced example, (ie. finding the youngest employee in the stream)
        System.out.println("\n Finding youngest employee================\n");
        departments.stream()
                .flatMap(department -> department.getEmployees().stream())
                .reduce((e1, e2) -> e1.getAge() < e2.getAge() ? e1 : e2)
                .ifPresent(System.out::println);
    }
}

package lambda;

interface CacheIterator {
    boolean hasNext();
    Bookmark next();
}

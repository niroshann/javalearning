package generics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenericsDemo {
    public static void main(String[] args) {
        Container<String> stringStore = new Store<>();
        stringStore.set("java");
        //stringStore.set(2); // this is not allowed as the stringStore variable is of type String
        System.out.println(stringStore.get());

        Container<Integer> integerStore = new Store<>();
        integerStore.set(1);
        //integerStore.set("something");  // this is not allowed as the integerStore variable is of type integer
        System.out.println(integerStore.get());


        Container<List<Integer>> listStore = new Store<>();
        listStore.set(Arrays.asList(1, 2, 3));
        System.out.println(listStore.get());

        List<Number> list = new ArrayList<>();
        list.add(new Integer(1));
        list.add(new Double(22.0));
        list.add(new Float(33.0));
        //list.add(new String("23.0")); // not allowed here, because we use generics and string isn't part of Number class


    }
}


interface Container<T>{
    void set(T a);
    T get();
}


class Store<T> implements Container<T> {
    private T a;

    @Override
    public void set(T a) {
        this.a = a;
    }

    @Override
    public T get() {
        return a;
    }
}

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

/**
 *
 * The filter API is normally used this way to reject wrapped values based on a predefined rule.
 * You could use it to reject a wrong email format or a password that is not strong enough.
 *
 * Let’s look at another meaningful example.
 * Let’s say we want to buy a modem and we only care about its price.
 * We receive push notifications on modem prices from a certain site and store these in objects:
 *
 * We then feed these objects to some code whose sole purpose is to check if the modem price is within our budget range for it.
 * We want to spend between ten to fifteen dollars on it. Let us now take a look at the code without Optional:
 *
 *
 * */

public class FilterApiExamples {
    public static void main(String[] args) {
    }

    public static boolean priceIsInRange1(Modem modem){
        boolean isInRange = false;

        // Pay attention to how much code we have to write to achieve this,
        // especially in the if condition.
        // The only part of the if condition that is critical to the application
        // is the last price-range check;
        // the rest of the checks are defensive
        if (modem != null && modem.getPrice() != null
                && (modem.getPrice() >= 10
                  && modem.getPrice() <= 15)) {

            isInRange = true;
        }
        return isInRange;
    }

    public static boolean priceIsInRange2(Modem modem2){

        return Optional.ofNullable(modem2)
                .map(modem -> modem.getPrice())
                .filter(p -> p >= 10)
                .filter(p -> p <=15)
                .isPresent();
    }


    // The map call is simply used to transform a value to some other value. Keep in mind that this operation does not modify the original value.
    // In our case, we are obtaining a price object from the Model class. We will look at the map API in detail in the next section.
    // First of all, if a null object is passed to this API, we don’t expect any problem.
    // Secondly, the only logic we write inside its body is exactly what the API name describes, price range check. Optional takes care of the rest:
    @Test
    public void whenFiltersWithoutOptional_thenCorrect(){
        Assert.assertTrue(priceIsInRange2(new Modem(10.0)));
        Assert.assertFalse(priceIsInRange2(new Modem(9.9)));
        Assert.assertFalse(priceIsInRange2(new Modem(null)));
        Assert.assertFalse(priceIsInRange2(new Modem(15.5)));
        Assert.assertFalse(priceIsInRange2(null));
    }

    // Test with priceIsInRange1()
    /*@Test
    public void whenFiltersWithoutOptional_thenCorrect(){
        Assert.assertTrue(priceIsInRange1(new Modem(10.0)));
        Assert.assertFalse(priceIsInRange1(new Modem(9.9)));
        Assert.assertFalse(priceIsInRange1(new Modem(null)));
        Assert.assertFalse(priceIsInRange1(new Modem(15.5)));
        Assert.assertFalse(priceIsInRange1(null));
    }*/
}


class Modem{

    private Double price;

    public Modem(Double price) {
        this.price = price;
    }

    public Double getPrice() {
        return price;
    }

}
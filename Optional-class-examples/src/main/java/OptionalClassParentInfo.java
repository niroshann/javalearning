import static org.junit.Assert.*;
import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.Optional;


/**
 *
 * Optional.ofNullable():
 * In case we expect some null values for the passed in argument, we can use 'Optional.ofNullable()'
 * it does not throw an exception but rather returns an empty Optional object as though we created it with the Optional.empty API
 *
 *
 *
 * Optional.of():
 * Use this method to create an 'Optional' object.
 * However, the argument passed to the of method cannot be null for 'Optional.of()' method,
 * If the argument is null then a Nullpointer exception will be thrown
 *
 *
 *
 * Optional.isPresent():
 * When there's an Optional object returned from an accessor method or created by us,
 * Its possible to check if there is a value in it or not with the isPresent() API:
 * This API returns "true" if and only if the value wrapped is not null
 *
 *
 * orElseGet() and orElse(): (This is debatable)
 * To retrieve the value wrapped inside an Optional instance advisable to use 'orElseGet()' instead of 'orElse()' see class 'DifferenceBetweenOrElseAndElseGetApi'
 *
 *
 *
 * orElseThrow():
 * The 'orElseThrow()' API follows from 'orElse()' and 'orElseGet()' in
 * It adds a new approach for handling an absent value.
 * Instead of returning a default value when the wrapped value is not present, it throws an exception,
 *
 *
 *
 * Optional.get():
 * Approach for retrieving the wrapped value is the get API
 * However, the get API can only return a value if the wrapped object is not null,
 * otherwise, it throws a no such element exception see Optional.get() example below.
 * This is the major flaw of the get API. Ideally, Optional should help us to avoid such unforeseen exceptions.
 * Therefore, this approach works against the objectives of Optional and will probably be deprecated in a future release.
 * It is, therefore, advisable to use the other variants which enable us to prepare for and explicitly handle the null case.
 *
 *
 *
 * Optional.filter():
 * The filter API is used to run an inline test on the wrapped value.
 * It takes a predicate as an argument and returns an Optional object.
 * If the wrapped value passes testing by the predicate, then the Optional is returned as is.
 * However, if the predicate returns false, then an empty Optional is returned:
 *
 * */

public class OptionalClassParentInfo {

    // 'Optional.of()' Example
    // The value of variable 'name' is available to process
    @Test
    public void givenNonNull_whenCreatesNonNullable_forOptionalOfMethod_thenCorrect(){
        String name = "baeldung";
        Optional<String> opt = Optional.of(name);
        System.out.println("value of 'opt' variable in method '"+ Thread.currentThread().getStackTrace()[1].getMethodName() + "' is: "+ opt);
        assertEquals("Optional[baeldung]", opt.toString());
    }

    // 'Optional.of()' Example
    // 'Name' variable with null value to it.
    // However, the argument passed to the of method cannot be null for 'Optional.of()' method,
    // otherwise, we will get a NullPointerException:
    @Test(expected = NullPointerException.class)
    public void givenNull_whenCreatesNonNullable_forOptionalOfMethod_thenCorrect(){
        String name = null;
        Optional<String> opt = Optional.of(name);
        System.out.println("value of 'opt' variable in method '"+ Thread.currentThread().getStackTrace()[1].getMethodName() + "' is: "+ opt);
        assertNotEquals("Optional[baeldung]", opt.toString());
    }


    // 'Optional.ofNullable()' Example
    // Passing 'non null' value for Optional.ofNullable()
    @Test
    public void givenNonNull_whenCreatesNullable_forOptionalOfNullableMethod_thenCorrect(){
        String name = "baeldung";
        Optional<String> opt = Optional.ofNullable(name);
        System.out.println("value of 'opt' variable in method '"+ Thread.currentThread().getStackTrace()[1].getMethodName() + "' is: "+ opt);
        assertEquals("Optional[baeldung]", opt.toString());
    }


    // 'Optional.ofNullable()' Example
    // But, in case we expect some null values for the passed in argument, we can use the ofNullable API:
    // In this way, if we pass in a null reference,
    // it does not throw an exception but rather returns an empty Optional object as though we created it with the Optional.empty API:
    @Test
    public void givenNull_whenCreatesNullable_forOptionalOfNullableMethod_thenCorrect(){
        String name = null;
        Optional<String> opt = Optional.ofNullable(name);
        System.out.println("value of 'opt' variable in method '"+ Thread.currentThread().getStackTrace()[1].getMethodName() + "' is: "+ opt);
        assertEquals("Optional.empty", opt.toString());
    }


    // 'Optional.isPresent()' Example
    // When there's an Optional object returned from an accessor method or created by us,
    // Its possible to check if there is a value in it or not with the isPresent() API:
    // This API returns "true" if and only if the value wrapped is not null
    @Test
    public void givenOptional_whenIsPresentWorks_thenCorrect(){
        Optional<String> opt = Optional.of("Baeldung");
        assertTrue(opt.isPresent());
        System.out.println("value of 'opt' variable in method '"+ Thread.currentThread().getStackTrace()[1].getMethodName() + "' is: "+ opt);

        opt = Optional.ofNullable(null);
        assertFalse(opt.isPresent());
        System.out.println("value of 'opt' variable in method '"+ Thread.currentThread().getStackTrace()[1].getMethodName() + "' is: "+ opt);
    }


    // 'Optional.empty()' and 'Optional.isPresent()' Example
    @Test
    public void whenCreatesEmptyOptional_thenCorrect(){
        Optional<String> empty = Optional.empty();
        System.out.println("value of 'empty' variable in method '"+ Thread.currentThread().getStackTrace()[1].getMethodName() + "' is: "+ empty.isPresent());
        assertFalse(empty.isPresent());
    }


    // In the below example, we use only two lines of code to replace the five that worked in the first
    // example. One line to wrap the object into an Optional object and
    // the next to perform implicit validation as well as execute the code
    @Test
    public void givenOptional_whenIfPresentWorks_thenCorrect(){
        Optional<String> opt = Optional.of("baeldung");
        opt.ifPresent(name -> System.out.println(name.length()));
    }


    // Default value with 'Optional.orElse()' Example
    // The orElse API is used to retrieve the value wrapped inside an Optional instance.
    // It takes one parameter which acts as a default value.
    // With orElse, the wrapped value is returned if it is present and
    // the argument given to orElse is returned if the wrapped value is absent
    @Test
    public void whenOrElseWorks_thenCorrect(){
        String nullName = null;
        String name = Optional.ofNullable(nullName).orElse("john");
        assertEquals("john", name);
    }


    // Default value with 'Optional.orElseGet()' Example
    // The orElseGet API is similar to orElse.
    // However, instead of taking a value to return if the Optional value is not present,
    // it takes a supplier functional interface which is invoked and returns the value of the invocation
    @Test
    public void whenOrElseGetWorks_thenCorrect(){
        String nullName = null;
        String name = Optional.ofNullable(nullName).orElseGet(() -> "john");
        assertEquals("john", name);
    }


    // 'orElseThrow' example
    // The 'orElseThrow' API follows from 'orElse' and 'orElseGet' in
    // It adds a new approach for handling an absent value.
    // Instead of returning a default value when the wrapped value is not present, it throws an exception:
    @Test(expected = IllegalArgumentException.class)
    public void whenOrElseThrowWorks_thenCorrect(){
        String nullName = null;
        String name = Optional.ofNullable(nullName).orElseThrow(IllegalAccessError::new);
    }


    // 'Optional.get()' Example
    // Approach for retrieving the wrapped value is the get API
    @Test
    public void givenOptional_whenGetsValue_thenCorrect(){

        Optional<String> opt = Optional.of("baeldung");
        String name = opt.get();
        assertEquals("baeldung", name);
    }


    // 'Optional.get()' Example
    // However, unlike the above three approaches, the get API can only return a value if the wrapped object is not null,
    // otherwise, it throws a no such element exception:
    // This is the major flaw of the get API. Ideally, Optional should help us to avoid such unforeseen exceptions.
    // Therefore, this approach works against the objectives of Optional and will probably be deprecated in a future release.
    //It is, therefore, advisable to use the other variants which enable us to prepare for and explicitly handle the null case.
    @Test(expected = NoSuchElementException.class)
    public void givenOptionalWithNull_whenGetThrowsException_thenCorrect(){
        Optional<String> opt = Optional.ofNullable(null);
        String name = opt.get();
    }

    @Test
    public void givenOptional_whenGetsValue_withOptionalOfNullable_thenCorrect(){
        Optional<String> opt = Optional.ofNullable(null);
        assertEquals(opt.get(), "John");
    }


    // 'Optional.filter()' Examples
    // The filter API is used to run an inline test on the wrapped value.
    // It takes a predicate as an argument and returns an Optional object.
    // If the wrapped value passes testing by the predicate, then the Optional is returned as is.
    // However, if the predicate returns false, then an empty Optional is returned:
    @Test
    public void whenOptionalFilterWorks_thenCorrect(){
        Integer year = 2016;
        Optional<Integer> yearOptional = Optional.of(year);

        System.out.println("'yearOptional' value in method "
                + Thread.currentThread().getStackTrace()[1].getMethodName() + " is " + yearOptional);

        boolean is2016 = yearOptional.filter(y -> y == 2016).isPresent();
        assertTrue(is2016);



        boolean is2017 = yearOptional.filter(y -> y == 2017).isPresent();
        assertFalse(is2017);
        System.out.println("'yearOptional' value in method "
                + Thread.currentThread().getStackTrace()[1].getMethodName() + " is " + yearOptional);
        //assertEquals(2017, yearOptional);

    }



}

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 *
 * In this example, we wrap a list of strings inside an Optional object and
 * use its map API to perform an action on the contained list.
 * The action we perform is to retrieve the size of the list.
 *
 * The map API returns the result of the computation wrapped inside Optional.
 * We then have to call an appropriate API on the returned Optional to retrieve its value.
 *
 * Notice that the filter API simply performs a check on the value and returns a boolean.
 * On the other hand, the map API takes the existing value,
 * performs a computation using this value and returns the result of the computation wrapped in an Optional object:
 *
 *
 * **/
public class TransformingValueWithMap {


    @Test
    public void givenOptional_whenMapWorks_thenCorrect() {
        List<String> companyNames = Arrays.asList("paypal", "oracle", "", "microsoft", "", "apple");
        Optional<List<String>> listOptional = Optional.of(companyNames);

        int size = listOptional
                .map(list -> list.size())
                .orElse(0);
        Assert.assertEquals(6, size);
    }

    @Test
    public void givenOptional_whenMapWorks_thenCorrect2() {
        String name = "baeldung";
        Optional<String> nameOptional = Optional.of(name);

        int len = nameOptional.map(s -> s.length())
                .orElse(0);

        Assert.assertEquals(8, len);
    }

    // We can chain map and filter together to do something more powerful.
    // Let’s assume we want to check the correctness of a password input by a user;
    // we can clean the password using a map transformation and check it’s correctness using a filter:

    //As it can be observed, without first cleaning the input,
    // it will be filtered out – yet users may take for granted that leading and trailing spaces all constitute input.
    // So we transform dirty password into a clean one with a map before filtering out incorrect ones.

    @Test
    public void givenOptional_whenMapWorksWithFilter_thenCorrect(){
        String password = " password ";
        Optional<String> passOpt = Optional.of(password);

        boolean correctPassword = passOpt
                .filter(s -> s.equals("password"))
                .isPresent();
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName() + "value:  " + correctPassword);
        Assert.assertFalse(correctPassword);

        correctPassword = passOpt
                .map(String::trim)
                .filter(s -> s.equals("password"))
                .isPresent();

        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName() + "value:  " + correctPassword);
        Assert.assertTrue(correctPassword);
    }


}

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

public class DifferenceBetweenOrElseAndElseGetApi {

    public String getMyDefault(){
        System.out.println("Getting default value");
        return "Default value";
    }

    @Test
    public void whenOrElseGetAndOrElseOverlap_thenCorrect() {
        String text = null;

        System.out.println("Using orElseGet:");
        String defaultText = Optional.ofNullable(text).orElseGet(this::getMyDefault);
        Assert.assertEquals("Default Value", defaultText);

        System.out.println("Using orElse:");
        defaultText = Optional.ofNullable(text).orElse(getMyDefault());
        Assert.assertEquals("Default Value", defaultText);
    }

    /*@Test
    public void whenOrElseGetAndOrElseDiffer_thenCorrect(){
        String text = "Text Present";

        System.out.println("Using orElseGet:");
        String defaultText = Optional.ofNullable(text).orElseGet(this::getMyDefault);
        Assert.assertEquals("Text Present", defaultText);

        System.out.println("Using orElse:");
        defaultText = Optional.ofNullable(text).orElse(getMyDefault());
        Assert.assertEquals("Text Present", defaultText);
    }*/

}

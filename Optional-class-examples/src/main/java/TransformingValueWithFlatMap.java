import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;


/***
 *
 * Just like the map API, we also have the flatMap API as an alternative for transforming values.
 * The difference is that:
 * "map" transforms values only when they are "unwrapped"
 * However "flatMap" takes a wrapped value and unwraps it before transforming it.
 *
 *
 * **/
public class TransformingValueWithFlatMap {

    @Test
    public void givenOptional_whenFlatMapWorks_thenCorrect2(){

        // Notice now that when we wrap a Person object, it will contain nested Optional instances:
        Person person = new Person("John", 26);
        Optional<Person> personOptional = Optional.of(person);

        Optional<Optional<String>> nameOptionalWrapper = personOptional
                .map(person1 -> person1.getName());

        Optional<String> nameOptional = nameOptionalWrapper
                .orElseThrow(IllegalArgumentException::new);

        String name1 = nameOptional.orElse("");
        Assert.assertEquals("John", name1);


        // Here, we’re trying to retrieve the name attribute of the Person object to perform an assertion.
        // Note how we achieve this with map API in the above statement and then notice how we do the same with flatMap API afterward.
        // The Person::getName method reference is similar to the String::trim call we had in the class "TransformingValueWithMaps" for cleaning up a password.
        // The only difference is that getName() returns an Optional rather than a String as did the trim() operation.
        // This, coupled with the fact that a map transformation result is wrapped in an Optional object leads to a nested Optional.
        // While using map API, therefore, we need to add an extra call to retrieve the value before using the transformed value.
        // This way, the Optional rapper will be removed. This operation is performed implicitly when using flatMap.

        String name = personOptional
                .flatMap(person1 -> person1.getName())
                .orElse("");
        Assert.assertEquals("John", name1);

    }
}

class Person{
    private String name;
    private int age;
    private String password;


    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Optional<String> getName() {
        return Optional.ofNullable(name);
    }

    public Optional<Integer> getAge() {
        return Optional.ofNullable(age);
    }

    public Optional<String> getPassword() {
        return Optional.ofNullable(password);
    }
}

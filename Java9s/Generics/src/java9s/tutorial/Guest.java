package java9s.tutorial;

public class Guest {
    public static void main(String[] args) {

        // Instantiating juice glass
        Glass<Juice> g = new Glass();
        Juice juice = new Juice();
        g.liquid = juice;
        Juice j = g.liquid;

        // Instantiating water glass
        Glass<Water> waterGlass = new Glass<>();
        waterGlass.liquid = new Water();
        Water water = waterGlass.liquid;

        // this is also correct
        /*Color<Red, Green, Blue> color = new Color<Red, Green, Blue>();*/
        Color<Red, Green, Blue> color = new Color<>();

        Bartender bartender = new Bartender();
        // this is also correct
        bartender.<Juice, Water>mix(juice, water);

        Glass2<OrangeJuice> orangeJuiceGlass = new Glass2<>();

        // appleJuiceGlass will not be instantiated because class AppleJuice doesn’t implement interface Liquid{}
        /*Glass2<AppleJuice> appleJuiceGlass = new Glass2<AppleJuice>();*/

        Glass<Juice> juiceGlass = new WineGlass<Juice>();

        // This is invalid because the types are different,
        // The defining side and the initialising side should contain the same type
        // Glass<Liquid> liquidGlass = new WineGlass<Juice>();

        Tray tray = new Tray();
        tray.add(new Glass<Juice>());

        // when instantiating wildcards aren't allowed,
        // because the runtime java needs to know the type argument. (ie which class to compile)
        // below is an example
        /*tray.add(new Glass<?>());*/

        // wild card '?' question mark with 'extends' keyword example
        // 'Orangejuice' is acceptable here,
        // because class 'OrangeJuice' extends class juice
        tray.add(new Glass<OrangeJuice>());

        // However 'Liquid' class as type parameter isn't allowed
        // because 'orangeJuice' class implements Liquid class which is upperbound
        /*tray.add(new Glass<Liquid>());*/

        // wild card '?' question mark with 'super' keyword example
        // Valid because cokeZero class extends cokeDiet
        tray.remove(new Glass<CokeDiet>());

        // However CokeGreen isn't allowed here
        // because CokeGreen inherits Coke class which is not part of CokeZero class hierarchy
        /*tray.remove(new Glass<CokeGreen>());*/

        // And also CokeXtra isn't allowed here
        // Eventhough CokeXtra inherits CokeZero class which is not part of jCokeZero class higher hierachy
        // See lower bounded wildcards
        //tray.remove(new Glass<CokeXtra>());
    }
}

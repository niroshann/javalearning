package java9s.tutorial;

public class Tray {

    // Generic variable example
    public Glass<?> genericVariableExample;

    // In this case any types can be passed through java9s.tutorial.Glass object.
    /*public void add(java9s.tutorial.Glass<?> glass){

        // local variable example this is not recommended
        java9s.tutorial.Glass<?> localVariableExample;
    }*/

    public void add(Glass<? extends Juice> glass){

        // local variable example this is not recommended
        Glass<?> localVariableExample;
    }

    public void remove(Glass<? super CokeZero> cokeGlass){

    }
}
